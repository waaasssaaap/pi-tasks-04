#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define N 256
#define SIZE 5		//quantity of strings

void RandOutp(char (*string)[N])
{
	int i, j, rnd;
	printf("\nRandomly output of strings:\n");
	for (i=0; i<SIZE; i++)
	{
		rnd=rand()%(SIZE-i);
		printf("%s",string[rnd]);
		strcpy(string[rnd], string[SIZE-1-i]);
	}
}

int main()
{
	char string[SIZE][N];
	int i;
	srand(time(NULL));
	printf("Enter %d strings:\n",SIZE);
	for (i=0; i<SIZE; i++)
		fgets(string[i],N,stdin);
	RandOutp(string);
	return 0;
}
