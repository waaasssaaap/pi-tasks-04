#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 256
#define SIZE 5	//quantity of strings

int main()
{
	char string[SIZE][N];
	int i;
	srand(time(NULL));
	printf("Enter %d lines: \n",SIZE);
	for (i=0; i<SIZE; i++)
		fgets(string[i],N,stdin);
	printf("\nRandom string:\n%s\n",string[rand()%SIZE]);
	return 0;
}