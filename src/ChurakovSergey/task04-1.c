#include <stdio.h>
#define N 15

int main()
{
	int i,j;
	i = 1;
	char str[N][256];
	fgets(str[0],256,stdin);
	while (str[i - 1][0] > 10 && i < N)
	{
		fgets(str[i], 256, stdin);
		i++;
	}
	int StrNum;
	StrNum = i - 2;
	for (i = StrNum;i >= 0;i--)
	{
		j = 0;
		while ((unsigned int)str[i][j] > 16)
		{
			putchar(str[i][j]);
			j++;
		}
		printf("\n");
	}
	return 0;
}