#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 15

void InsSort(int* arr[], int* index[], int size)
{
	int i, j, temp,temp2;
	for (i = 1;i < size;i++)
	{
		j = i;
		temp = (int)arr[i];
		temp2 = (int)index[i];
		while (j > 0 && temp < (int)arr[j - 1])
		{
			arr[j] = arr[j - 1];
			index[j] = index[j - 1];
			j--;
		}
		arr[j] =(int*)temp;
		index[j] = (int*)temp2;
	}
}

int main()
{
	int i, j, temp, sym;
	int* index[N];
	char str[N][256];
	int* arr[N];
	for (i = 0;i < N;i++)
		index[i] = (int*)i;
	i = 1;
	fgets(str[0], 256, stdin);
	while (str[i - 1][0] > 10 && i < N)
	{
		fgets(str[i], 256, stdin);
		i++;
	}
	for (j = 0; j < i - 1; j++)
		arr[j] =(int*) strlen(str[j]);
	InsSort(arr,index, i - 1);
	for (j = 0;j < i - 1;j++)
	{
		sym = 0;
		while (str[((int)index[j])][sym] > 10)
		{
			putchar(str[((int)index[j])][sym]);
			sym++;
		}
		printf("\n");
	}
	return 0;
}